// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import IPad from './components/IPad'
import SuiVue from'semantic-ui-vue'
import './assets/semantic-ui-css/semantic.min.css'
import axios from 'axios'
import router from './router'
console.log(SuiVue)
Vue.config.productionTip = false
Vue.use(SuiVue)
Vue.prototype.$eventHub = new Vue(); // Global event bus
Vue.prototype.$axios = axios;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
