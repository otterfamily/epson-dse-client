
import Vue from 'vue'
import Router from 'vue-router'
import IPad from '@/components/IPad'
import Bar from '@/components/Bar'
import Marquee from '@/components/Marquee'
import Selector from '@/components/Selector'

Vue.use(Router)
export default new Router({
  mode:'hash',
  routes: [
    {
      path: '/',
      name: 'selector',
      component: Selector
    },
    {
      path: '/ipad/:ip',
      name: 'ipad',
      component: IPad
    },
    {
      path: '/marquee/:ip',
      name: 'marquee',
      component: Marquee
    },
    {
      path: '/bar/:ip',
      name: 'bar',
      component: Bar
    }
  ]
})